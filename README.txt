
How to use this module
-----------------------

1. Put the module in your modules folder and enable.
2. Go to admin/settings/splashpage_redirect and fill out the settings.
3. If you want to give the user an option to permanently skip, make sure the
   splashPageRedirectSkipPermanent javscript function is called for the user
   on the splash page.

