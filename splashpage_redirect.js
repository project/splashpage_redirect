
/**
 * Get the cookie value for this page.
 */
function splashPageRedirectCookieGet(cookieName) {
  var cookies = document.cookie.split(';');
  for (var i=0; i < cookies.length; i++) {
    var cookie = cookies[i].split('=');
    // Trim by Crockford http://javascript.crockford.com/remedial.html
    if (cookie[0] && cookie[1] && cookie[0].replace(/^\s+|\s+$/g, "") == cookieName) {
      return decodeURIComponent(cookie[1]);
    }
  }
  return false;
}

/**
 * Increment the cookie value for this page.
 */
function splashPageRedirectCookieIncrement() {
  var cookieName = Drupal.settings.splashPageRedirect.cookieName;
  var cookieValue = splashPageRedirectCookieGet(cookieName);
  if (!cookieValue) {
    cookieValue = 1;
  }
  else {
    cookieValue++;
  }
  document.cookie = cookieName + '=' + cookieValue + '; expires=Thu, 01 Jan 2060 05:00:00 GMT; path=/';
  return cookieValue;
}

function splashPageRedirectSkipPermanent() {
  var cookieName = Drupal.settings.splashPageRedirect.cookieSkipName;
  document.cookie = cookieName + '=1; expires=Thu, 01 Jan 2060 05:00:00 GMT; path=/';
  return false;
}

if (Drupal.settings.splashPageRedirect.url && !window.location.search.match('[?&]noredirect')) {
  var cookieValue = splashPageRedirectCookieIncrement();
  // Skip if the user has reqeuested to always skip or the user has viewed
  // more than the required # of times.
  if (cookieValue > Drupal.settings.splashPageRedirect.views || splashPageRedirectCookieGet(Drupal.settings.splashPageRedirect.cookieSkipName)) {
    window.location.replace(Drupal.settings.splashPageRedirect.url);
  }
}